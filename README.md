# Node Express MySQL #
## 1.0.0 ##

This is a sample REST API app using NodeJS, ExpressJS and MySQL

## Requirements ##

* MySQL - https://dev.mysql.com/downloads/installer/
* Node.js - https://nodejs.org/en/download/

## MySQL Setup ##

Create user_db database and users table -

```
#!sql

CREATE DATABASE `user_db`;

USE `user_db`;

CREATE TABLE `users` (
	`id` BIGINT AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `email` VARCHAR(100) NOT NULL,
    `active` BOOLEAN DEFAULT 1,
    `created_at` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(`id`)
)
```

## How to run? ##
 
  * Install node dependencies - ```npm install```
  * Run App - ```npm start``` OR ```node server.js```
  
Once the application is up, you may access it from http://localhost:8888/users