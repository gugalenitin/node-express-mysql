// Basic Setup
var express  = require('express');
var mysql    = require('mysql');
var parser   = require('body-parser');
var cors     = require('cors');
 
// Database Connection
var connection = mysql.createConnection({
  host     : 'localhost',
  port     : 3306,
  user     : 'root',
  password : '',
  database : 'user_db'
});
try {
    connection.connect();
    console.log('Database connection successful!');
} catch(e) {
	console.log('Database Connetion failed:' + e);
}
 
// Setup express
var app = express();
var port = 8888;
app.use(parser.json());
app.use(parser.urlencoded({ extended: true }));
app.use(cors());
app.listen(port);

console.log('Users RESTful API server started on: ' + port);
 
// Set default route
app.get('/', function (req, res) {
	res.send('Hello World!');
});

app.get('/users', function (req, res) {
    connection.query('SELECT * FROM users',
        function (error, results, fields) {
            if (!error) {
                res.status(200).json({ result: 'success', data: results });
            } else {
                res.status(400).send(error);
            }
        });
});

app.post('/users', function (req, res) {
	if (req.body.name !== undefined && req.body.email !== undefined) {
		connection.query('INSERT INTO users (name, email) VALUES (?, ?)', 
			[req.body.name, req.body.email], 
			function(error, result) {
		  		if (!error) {
			    	res.status(200).json({ result: 'success' });
		  		} else {
				    res.status(400).send(error);
			  	}
			});
	} else {
		res.status(200).send({ result: 'error', msg: 'Please fill required details'});
	}
});

app.put('/users/:user_id', function (req, res) {
	if (req.params.user_id !== undefined && req.body.name !== undefined && req.body.email !== undefined) {
		connection.query('UPDATE users SET name = ?, email = ? WHERE id = ?', 
			[req.body.name, req.body.email, req.params.user_id], 
			function(error, result) {
		  		if (!error) {
			    	res.status(200).json({ result: 'success' });
		  		} else {
				    res.status(400).send(error);
			  	}
			});
	} else {
		res.status(200).send({ result: 'error', msg: 'Please fill required details'});
	}
});

app.delete('/users/:user_id', function (req, res) {
	if (req.params.user_id) {
		connection.query('DELETE FROM users WHERE id = ?', 
			[req.params.user_id], 
			function(error, result) {
		  		if (!error) {
			    	res.status(200).json({ result: 'success' });
		  		} else {
				    res.status(400).send(error);
			  	}
			});
	} else {
		res.status(200).send({ result: 'error', msg: 'Please fill required details'});
	}
});
